# Analysis I & II FS
Diese Zusammenfassung gehört zu den Vorlesungen "Analysis I" und "Analysis II" (D-ITET) von Alessandra Iozzi.
Wenn ihr irgendwelche Änderungsvorschläge habt (oder wenn ihr Fehler gefunden habt) könnt ihr mich entweder per Mail erreichen oder ihr könnt hier ein "Issue" eröffnen.
[phsauter@student.ethz.ch](phsauter@student.ethz.ch)
## Disclaimer
Das Layout wurden der Zusammenfassung von Theo von Arx (HS16-FS17; Analysis) entnommen.
Einige Befehle etc. wurden von der Zusammenfassung von Silvano Cortesi (Analysis) übernommen.
Auch der Inhalt bassiert zu grossen Teilen auf der Zusammenfassung von The von Arx (HS16-FS17; Analysis).
Er wurde mit Hilfe des Skripes von A. Iozzi und Wikipedia ergänzt
**Es ist gut möglich, dass die Zusammenfassung inhaltliche Fehler aufweist.**
