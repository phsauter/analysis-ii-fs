\section{Differentialrechnung in mehreren Variablen}
	\subsection{Grundbegriffe}
		Sei $f: \Omega \rightarrow \real \qquad (x_1, \ldots, x_n) \mapsto f(x_1, \ldots, x_n)$.\\
		\textbf{Partielle Ableitung}:
		Man betrachtet alle Variablen ausser jene, nach der man ableiten möchte, als Konstanten und führt die Ableitung
		wie bei einer Funktion einer Variablen durch. Schreibweise:
		\cenv{$\left.\frac{\partial f}{\partial x_i}\right\vert_{(\vec{x}_0)}
			= f_{x_i}(\vec{x}_0) \qquad (1 \leqslant i \leqslant n)$}
		ist die partielle Ableitung von $f$ nach $x_i$ an der Stelle $\vec{x}_0$.

		\textbf{Richtungsableitung}:
		Wie die partielle Ableitung, allerdings kann nicht nur in Richtung der Koordinaten
		sondern in beliebige Richtungen abgeleitet werden.
		\cenv{$D_{\vec{e}}f(\vec{x}_0) := \lim_{t \rightarrow 0^+} \frac{f(\vec{x}_0 + t\vec{e}) - f(\vec{x}_0)}{t}
		= \nabla f(\vec{x}) \dotprod \vec{e}$}
		ist die Richtungsableitung von $f$ an der Stelle $\vec{x}_0$ in Richtung $\vec{e}$.
		\hlt{hlyellow}{$\vec{e}$ ist ein Einheitsvektor!}\\

		\textbf{Klassen}: Eine Funktion $f$ ist eine $C^n$-Funktion, wenn sie $n$-te stetige partielle Ableitungen
		nach \ul{allen} Variablen besitzt.\\
		\textbf{Vertauschbarkeit}: Sei $f$ eine Funktion der Klasse $C^2$. Dann ist die Reihenfolge der partiellen Ableitungen irrelevant:
		\fcenv{$\frac{\partial^2 f}{\partial x_i \partial x_j} = \frac{\partial^2 f}{\partial x_j \partial x_i}$}
		
		
	\subsection{Nabla-Operator}
		Der Nabla Operator $\nabla$ ist ein Vektor-Operator der hauptsächlich im Kontext des Gradienten, der Divergenz und der Rotation benutzt wird.\\
		Der Nabla Operator ist ein Vektor bestehend aus den partiellen Ableitungen nach allen Variablen.\\
		\cenv{$\nabla = \brackets{\frac{\partial}{\partial x_1}, \dots , \frac{\partial}{\partial x_n} }$}
		Der Nabla Operator kann je nach Situation als Spalten oder Zeilenvektor intepretiert werden (bei $\grad$ als Spaltenvektor; bei $\div$ als Zeilenvektor).
		
		\subsubsection{Laplace-Operator}
			Mithilfe des Nabla Operators kann auch der Laplace Operator $\laplace$ definiert werden, er entspricht der Divergenz des Gradienten.\\
			\cenv{$\laplace = \div(\grad f) = \nabla \cdot \nabla = \brackets{\frac{\partial^2}{\partial x_1^2}, \dots , \frac{\partial^2}{\partial x_n^2}}$}
		

	\subsection{Gradient}
		Sei $f: \real^n \supset \Omega \rightarrow \real$. Dann ist
		\cenv{$\grad f(\vec{x}) = \nabla f(\vec{x})
		:= \left(\frac{\partial f(\vec{x})}{\partial x_1}, \ldots, \frac{\partial f(\vec{x})}{\partial x_n} \right)$}
		der Gradient von $f$ an der Stelle $\vec{x}$. Der Gradient zeigt in die Richtung der maximalen Zuwachsrate
		von $f$ an der Stelle $\vec{x}$ und sein Betrag ist die maximale Zuwachsrate.\\
		
		$f(x)$ in schwarz und das Gradientenfeld $\grad f(\vec{x})$ in blau.
		\cenv{\includegraphics[width=0.5\columnwidth,keepaspectratio=true]{\imgPath gradient.png}}


	\subsection{Lineare Approximation}
		Sei $f: \real^n \supset \Omega \rightarrow \real$ eine $C^1$-Funktion
		und $\vec{x} = (x_1, \ldots, x_n) \in \Omega$ ein fester Punkt. Dann gilt
		\cenv{$f(\vec{x} + \vec{h}) - f(\vec{x}) \doteq \nabla f(\vec{x}) \dotprod \vec{h}$}
		Der Gebrauch dieser Näherung bedeutet, dass man sich statt auf dem Graph von $f$ auf dessen \textbf{Tangentialebene} $T$ bewegt.
			
								
	\subsection{Kurven}
		Eine \textbf{Kurve} ist eine \ul{stetige} Abbildung
		\cenv{$\vec{f}: I \rightarrow \real^n \qquad t \mapsto (f_1(t), \ldots, f_n(t))$}
		wobei $I \subset \real$ ein Intervall ist. Sie ist (stetig) diff'bar, wenn alle Komponenten (stetig) diff'bar sind.\\
		Ein Punkt $\vec{f}(t)$ auf einer Kurve heisst \textbf{singulär}, falls $\vec{f}'(t) = 0$ ist, sonst \textbf{regulär}.
		Die Kurve heisst \textbf{regulär}, falls $\vec{f}'(t) \neq 0 ~ \forall t \in I$.\\
		Die \textbf{Ableitung} einer Kurve erhält man durch Ableitung aller Komponenten.

		\subsubsection{Länge einer Kurve}
			Das \textbf{Linienelement} einer Kurve $\vec{r}(t)$ ist
			\cenv{$ds := \Vert \vec{r}' (t) \Vert ~ dt$}
			Spezialfall: Die Kurve ist Graph einer Funktion $y = f(x)$:
			\cenv{$ds = \sqrt{1 + y'^2} ~ dx$}
			Daraus ergibt sich die Länge der Kurve zu
			\cenv{$\int_a^b ~ ds = \int_a^b \Vert \vec{r}' (t) \Vert ~ dt$}

		\subsubsection{Verallgemeinerte Kettenregel}
			Sei $\vec{x}: \real \rightarrow \Omega \subset \real^n$ eine Kurve,
			und sei $f: \Omega \rightarrow \real$ eine Funktion.\\
			Sei $ \phi(t) := f(\vec{x}(t))$. Wenn $\vec{x}$ und $f$ stetig partiell differenzierbar sind, dann auch $\phi$, und es ist
			\fcenv{$\phi'(t) = \frac{\partial}{\partial t} f(\vec{x}(t)) = \nabla f(\vec{x}(t)) \dotprod \vec{x}' (t)$}

		\subsubsection{Differentiation unter dem Integralzeichen}
			Sei $f(\; \cdot \;,t): [a(t), b(t)]\rightarrow \real, \; x \mapsto f(x,t)$ mit $t$ parametrisiert.\\
	
			Falls $a$ und $b$ von $t$ abhängen, definiere
			\cenv{$\Phi(t) := \int^{b(t)}_{a(t)}f(x,t) \; dx$}
			Dann ist 
			\fcenv{$\Phi'(t) = f(b(t),t)\cdot b'(t) - f(a(t),t)\cdot a'(t) + \int^{b(t)}_{a(t)}\frac{\partial f(x,t)}{\partial t} \; dx$}
	
			Falls $a$ und $b$ \ul{nicht} von $t$ abhängen, dann ist
			\fcenv{$\frac{d}{dt} \int_a^b f(x,t) \; dx = \int_a^b \frac{\partial f(x,t)}{\partial t} \; dx$}


	\subsection{Satz über implizite Funktion}
		Sei $L:=\left\{ (x,y) \in \real^2 \vert f(x,y)=0 \right\}$.\\
		Wenn für einen Punkt $(x_0,y_0) \in L\) gilt: \(f_y (x_0,y_0) \neq 0 $ so existiert eine differenzierbare Funktion
		$x \mapsto y := \phi(x) $, die lokal (mit Zentrum $(x_0,y_0)$) der Graph der Lösungsmenge $L$ ist. \\
		Lokal gilt $f(x,\phi(x)) = 0$. Aus der allgemeinen Kettenregel folgt \\
		\fcenv{$\phi'(x) = -\dfrac{f_x(x,y)}{f_y(x,y)}$}
		\cenv{$\phi''(x) = \dfrac{-f_{xx}{f_{y}}^2 + 2 f_x f_y f_{xy} - f_{yy} {f_{x}}^2}{{f_{y}}^3}$}
		\begin{quote}
			Wenn die Ableitung nach $x_i$ nicht null ist, lässt sich die Funktion zumindest lokal nach $x_i$ auflösen.
		\end{quote}

		\subsubsection{Niveaulinien}
			Die Menge $N_f(C) := \{(x,y) \in \real^2 \supset \Omega ~ \vert ~ f(x,y) = C\}$ 
			(die Menge aller Punkte, in denen $f$ den Wert $C$ annimmt) ist die \textbf{Niveaulinie} von $f$ zum Niveau $C$.\\
			Sei $\vec{z}_0 = (x_0,y_0)$ ein regulärer Punkt der $C^1$-Funktion
			$F: \real^2 \to \real$ und $f(\vec{z}_0) =: C_0$. Dann ist die Niveaulinie $N_f(C_0)$ in der Umgebung von
			$\vec{z}_0$ eine glatte Kurve und der Gradient $\nabla f(\vec{z}_0)$ steht senkrecht auf der Tangente
			an diese Kurve im Punkt $\vec{z}_0$.

		\subsubsection{Niveauflächen}
			Die Menge $N_f(C) := \{(x,y,z) \in \real^3 \supset \Omega ~ \vert ~ f(x,y,z) = C\}$  (die Menge aller Punkte,
			in denen $f$ den Wert $C$ annimmt) ist die \textbf{Niveaufläche} von $f$ zum Niveau $C$.\\
			Sei $\vec{z}_0 = (x_0,y_0, z_0)$ ein regulärer Punkt der $C^1$-Funktion
			$F: \real^3 \to \real$ und $f(\vec{z}_0) =: C_0$. Dann ist die Niveaufläche $N_f(C_0)$ in der Umgebung von
			$\vec{z}_0$ eine glatte Fläche $S$ und der Gradient $\nabla f(\vec{z}_0)$ steht senkrecht auf der Tangentialebene
			$T_{\vec{z}_0}S$.


	\subsection{Taylorentwicklung in zwei Variablen}
		Das \textbf{Taylorpolynom} in zwei Variablen vom Grad $N$ um den Entwicklungspunkt $(x_0,y_0)$ ist
		\cenv{$j_{(x_0,y_0)}^N f(\Delta x,\Delta y):= \sum\limits_{r=0}^{N} \frac{1}{r!}
			\cdot \left(\sum\limits_{k=0}^{r} \binom{r}{k} \frac{\partial^r f}{\partial^{r-k} x ~\partial^{k}y}(x_0, y_0)
		\cdot \Delta x^{r-k} \cdot \Delta y^k \right)$}
		(Binomialkoeffizient ist da, damit identische partielle Ableitungen gleich mehrfach gezählt werden.
		Das Ganze kann auch mit zwei verschachtelten Summen geschrieben werden, siehe Hessesche Form.)\\
		\emph{Spezialfall $N=2$:}
		\cenv{$j_{x_0,y_0}^2 f(\Delta x,\Delta y) =
			f(x_0, y_0) + \frac{\partial f(x_0,y_0)}{\partial x}\Delta x +\frac{\partial f(x_0,y_0)}{\partial y}\Delta y
			+ \frac{1}{2}\frac{\partial^2 f(x_0,y_0)}{\partial^2 x}\Delta x^2
			+\frac{1}{2}\frac{\partial^2 f(x_0,y_0)}{\partial^2 y}\Delta y^2
		+\frac{\partial^2 f(x_0,y_0)}{\partial x \partial y}\Delta x \Delta y$}
		Sei $f : \Omega \to \real$ eine Funktion der Klasse $C^{N+1}$ , wobei $\Omega \subset \real^2$ eine offene Teilmenge ist.
		Sei $\vec{z}_0 \in \Omega$ und $\vec{z} = \vec{z}_0 + \Delta \vec{z} \in \Omega$. Dann gilt
		\cenv{$f(\vec{z}) = j_{\vec{z}_0}^N f(\Delta \vec{z}) + o(\Vert\Delta \vec{z}\Vert^N)$}
		Das $N$-te \textbf{Restglied} berechnet sich mit
		\cenv{$R_N = \frac{1}{(N+1)!}
			\cdot \left| \sum\limits_{k=0}^{N+1}\binom{N+1}{k}
			\frac{\partial^{N+1}f(x_0+\tau \Delta x, y_0+\tau \Delta y)}{\partial x^{N+1-k} ~\partial y^k}
		\cdot \Delta x^{N+1-k} \Delta y^k \right|$}

		\subsubsection{Hessesche Form}
		\label{sec:hessesche-form}
			$T_{f(a)}^2(x,y,z) = f(a) + \diff f(a) \left( \begin{array}{c}
					x \\ y \\
			z  \end{array}\right)
			+ \frac{1}{2}
			\left( \begin{array}{c}
					x \ y \ z
			\ \end{array}\right) \diff^2f(a)
			\left( \begin{array}{c}
					x
					\\ y
			\\ z  \end{array}\right)$


	\subsection{Funktionalmatrix \& Differenzierbarkeit}
		\cenv{$\left[\frac{\partial\vec{f}}{\partial\vec{x}}\right]_{\vec{p}} :=
			\left[\frac{\partial(f_1,\ldots ,f_m)}{\partial(x_1,\ldots ,x_n)}\right] :=
			\left[
				\begin{array}{lll}
					\frac{\partial f_1}{\partial x_1}		& \ldots	&	\frac{\partial f_1}{\partial x_n}\\
					\vdots									& \ddots	&	\vdots\\
					\frac{\partial f_m}{\partial x_1}		& \ldots	&	\frac{\partial f_m}{\partial x_n}\\
				\end{array}
		\right]$}
		ist die \textbf{Funktional-} oder \textbf{Jacobimatrix} von $\vec{f}$ an der Stelle $\vec{p}$.
		Die Zeilen der Funktionalmatrix sind die Gradienten der Koordinatenfunktionen, die Spalten sind die Ableitungsvektoren
		der partiellen Funktionen.\\

		\subsubsection{Kettenregel}
			Es seien
			\cenv{$\vec{y}(\cdot): ~ \real^n \to \real^m, ~ \vec{x} \mapsto \vec{y}(\vec{x})$}
			\cenv{$\vec{z}(\cdot): ~ \real^m \to \real^s, ~ \vec{y} \mapsto \vec{z}(\vec{y})$}
			zwei $C^1$-Abbildungen mit $\vec{y}(\vec{p}) =: \vec{q} \in \dom{(\vec{z}(\cdot))}$.
			Dann besitzt ihre Zusammensetzung $\vec{x} \mapsto \vec{z}(\vec{y}(\vec{x}))$ die folgende Funktionalmatrix:
			\cenv{$\left[\frac{\partial\vec{z}}{\partial\vec{x}}\right]_{\vec{p}}
				= \left[\frac{\partial\vec{z}}{\partial\vec{y}}\right]_{\vec{q}}
				\cdot
			\left[\frac{\partial\vec{y}}{\partial\vec{x}}\right]_{\vec{p}}$}


	\subsection{Umkehrsatz}
	\label{sec:umkehrsatz}
		Ist $\det (df(x_0) \neq 0$, so ist $f$ lokal umkehrbar, d.h. es
		existieren offene Umgebungen $U$ von $x_0$ und $V$ von $f(x_0)$ sodass
		$f$ eingeschränkt auf $U$ bijektiv ist. Für die Ableitung gilt:
		\cenv{$d(f^{-1})(y) = (df(x))^{-1} = \textnormal{ Inverse der
		Jacobi-Matrix von $f$, wobei } y = f(x).$}

		\bigskip

		Eine Abbildung $f:\Omega\rightarrow f(\Omega)$ heisst
		\textbf{Diffeomorphismus}, falls
		\begin{citemize}
			\item $f$ ist bijektiv,
			\item $f$ ist $C^1$,
			\item die Umkehrabbildung $f^{-1}$ ist $C^1$.
		\end{citemize}

		Der Satz besagt also: Falls det$df(x)\neq 0$ für alle $x\in\Omega$, so
		hat jeder Punkt $x\in\Omega$ eine Umgebung $U$ von $x$, sodass $f$ ein
		Diffeomorphismus von $U$ nach $f(U)$ ist.


	\subsection{Satz über inverse Funktionen}
	\label{sec:satz-uber-inverse}
		Es sei $D\subset \real^n$ offen und $f:D\rightarrow\real^n$,
		$f\in C^k$, so dass die Ableitung d$f(a)$ für ein $a\in D$ invertierbar
		ist. Dann gibt es offene Mengen $U\in D$ und $V\in\real^n$ mit
		$a\in U$ und $b:= f(a) \in V$, so dass $f:U\rightarrow V$ bijektiv ist
		und die Umkehrabbildung $f^{-1}:V\rightarrow U$ ebenfalls $C^k$ ist.


	\subsection{Homöomorphismus}
	\label{sec:homoomorphismus}
		Eine Abbildung $f:\Omega\rightarrow f(\Omega)$ heisst
		\textbf{Homöomorphismus}, falls
		\begin{citemize}
			\item $f$ ist bijektiv,
			\item $f$ ist $C^0$,
			\item die Umkehrabbildung $f^{-1}$ ist $C^0$.
		\end{citemize}
	
	\subsection{Finden eines Berührungspunktes}
		Gegeben sind zwei Kurven/Flächen in der Form $F(\vec{x}) = 0$.\\
		Ein Berührungspunkt hat zwei essentielle Eigenschaften:\\
		\begin{citemize}
			\item Die zwei Kurven gehen durch den gleichen Punkt ($\vec{x_1} = \vec{x_2}$)
			\item Die zwei Kurven haben in diesem Punkt die gleiche Tangente
				\cenv{$\nabla F_1(\vec{x}) = \lambda\cdot F_2(\vec{x})$}
		\end{citemize}


	\subsection{Analyse von kritischen Punkten}
		Ein Punkt $\vec{p} \in \dom{f}$ heisst \textbf{kritisch} oder \textbf{stationär}, wenn:
		\fcenv{$\nabla f(\vec{p}) = \vec{0}$}
		d.h. bei einer infinitesimalen Änderung der Koordinaten bleibt der Funktionswert näherungsweise konstant.

		\subsubsection{Hessematrix (bestimmen der Art des kritischen Punktes)}
		%%\eqbox{H(\Delta x_1, \ldots, \Delta x_n) := \sum_{i=1}^n \sum_{k=1}^n f_{ik} \Delta x_i \Delta x_k}
		In zwei Variablen ist
		%\eqbox{H(\Delta x, \Delta y) = f_{xx}\Delta x^2 + 2 f_{xy}\Delta x \Delta y + f_{yy}\Delta y^2}
		\cenv{$H_f = \left[\begin{matrix}
					\frac{\partial^2}{\partial x \partial x} f	&	\frac{\partial^2}{\partial x \partial y} f\\
					\frac{\partial^2}{\partial y \partial x} f	&	\frac{\partial^2}{\partial y \partial y} f
		\end{matrix}\right]$}
		\cenv{$\det H = f_{xx} f_{yy} - f_{xy}^2$}
		Ein kritischer Punkt $\vec{z}_0$ mit $\det H \vert_{\vec{z}_0} \neq 0$ heisst \textbf{nichtentartet}. Die Hessematrix
		zeigt, ob es sich bei dem nichtentarteten kritischen Punkt um einen Sattelpunkt oder ein lokales Extremum handelt:\\
		\begin{tabular}{ll}
			\emph{Sattelpunkt}			&		$f_{xx}f_{yy}-f^2_{xy} < 0$\\
			\emph{Lokales Extremum}		&		$f_{xx}f_{yy}-f^2_{xy} > 0$\\
			~~~\emph{Maximum}			&		$f_{xx} < 0$\\
			~~~\emph{Minimum}			&		$f_{xx} > 0$\\
		\end{tabular}
		\bigskip

		Definition mit Definitheit:\\
		$H_f(x=a)$ ist:
		\begin{tabular}{lll}
			positiv definit    & EW $> 0$    & Minimum\\
			negativ definit    & EW $< 0$    &
			Maximum\\
			indefinit          &           &
			Sattelpunkt\\
			positiv semidefinit & EW $\geq$ 0 &
			Minimum
			oder
			Sattelpunkt\\
			negativ semidefinit & EW $\leq 0$ &
			Maximum
			oder
			Sattelpunkt\\
		\end{tabular}

		\subsubsection{Geometrisches Extremalproblem}
			Gesucht seien die globalen Extrema einer Funktion $f$ auf einem $n$-dimensionalen Bereich $B$.\\
			\emph{Rezept}:
			\begin{cenumerate}
				\item	Suche allen kritischen Punkten im Inneren von $B$ ($\nabla f(\vec{p}) = \vec{0}$)
				\item	Suche alle kritischen Punkten im "relativen Innern" jeder $d$-dimensionalen Seitenfläche, $0 < d < n$,
				\item	Suche alle Eckpunkten
				\item	Berechne den Funktionswert in jedem Punkt
				\item	Wähle das Maximum / Minimum aus dieser Liste von Punkten
			\end{cenumerate}								

		\subsubsection{Methode von Lagrange}
		\begin{cenumerate}
			\item	Zu maximierende / minimierende Funktion: $f(x_1,x_2,\dots,x_n)$\\
				\hlt{hlyellow}{Monotonie der Funktion beachten! $f$ kann ggf. vereinfacht werden!}\\
				$f = f(g(\vec{x}))$, ist f(x) streng steigend $\to g(\vec{x})$ max/min\\
				$f = f(g(\vec{x}))$, ist f(x) streng fallend $\to g(\vec{x})$ min/max \emph{(es dreht!)}\\
			\item	Nebenbedingungen in "gleich-Null"-Form bringen:
				\begin{align*}
					F_1(x_1,x_2,\dots,x_n) &= 0\\
					F_2(x_1,x_2,\dots,x_n) &= 0\\
					\vdots\\
					F_m(x_1,x_2,\dots,x_n) &= 0
				\end{align*}
			\item	Lagrange-Funktion aufstellen:
				\begin{align*}
					\Phi(\vec{x},\vec{\lambda})	&= f(\vec{x})\\
												&-\lambda_1 \cdot F_1(\vec{x})\\
												&-\lambda_2 \cdot F_2(\vec{x})\\
												&-\dots\\
												&-\lambda_m \cdot F_m(\vec{x})
				\end{align*}
			\item	Gleichungssystem lösen:
				\begin{align*}
					\frac{\partial}{\partial x_1} \Phi(\vec{x},\vec{\lambda}) &= 0\\
					\frac{\partial}{\partial x_2} \Phi(\vec{x},\vec{\lambda}) &= 0\\
					\vdots\\
					\frac{\partial}{\partial x_n} \Phi(\vec{x},\vec{\lambda}) &= 0\\
					F_1(\vec{x}) &= 0\\
					F_2(\vec{x}) &= 0\\
					\vdots\\
					F_m(\vec{x}) &= 0
				\end{align*}
				Bei nichtlinearen LGS: Fallunterscheidung! Was kann man alles nullsetzen? Welche Fälle kann man ausschliessen wegen Widersprüchen?\\
				Manchmal ist es hilfreich, alle Gleichungen nach $\lambda$ aufzulösen und gleichzusetzen.
			\item	Maximal- oder Minimalproblem? Dementsprechend maximale oder minimale Lösungen nehmen.
		\end{cenumerate}

		% \subsection{Kurvenscharen}
		% 	\subsubsection{Orthogonaltrajektorien}
		% 		\emph{Rezept}:
		% 		\begin{cenumerate}
		% 			\item	Finde DGL der Kurvenschar
		% 			\item	Finde $y'$ der Orthogonaltrajektorien: $y_\perp ' = - \frac{1}{y'}$
		% 			\item	Bringe alle $y$ auf eine Seite und integriere beidseitig nach $x$.
		% 			\item	Schreibe $y$ als $y(x)$, substituiere
		% 						\eqbox{u := y(x), ~ \frac{du}{dx} = y'(x), ~ \left(dx = \frac{du}{y'(x)}\right)}
		% 			\item	Rücksubstitution, $+ C$ nicht vergessen!
		% 		\end{cenumerate}		

