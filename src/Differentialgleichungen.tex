\section{Gewöhnliche Differentialgleichungen}
	\subsection{Allgemeines}
		Die Lösungen einer gewöhnlichen DGL sind Funktionen \emph{einer} Variablen.
		Die \textbf{Ordnung} einer DGL ist die Ordnung der höchsten in der DGL auftretenden Ableitung.
		Eine DGL ist \textbf{linear} wenn die gesuchte Funktion und alle ihre Ableitungen nur in linearen Termen vorkommen.
		Eine \textbf{homogene} DGL enthält nur Terme, die die unbekannte Funktion oder ihre Ableitungen enthalten.
		Die allgemeine Lösung einer inhomogenen linearen DGL ist die Summe der Lösung des homogenen Problems
		und der \textbf{partikulären Lösung} des inhomogenen Problems:
		\cenv{$y(x) = y_h(x) + y_p(x)$}


	\subsection{Lineare DGL 1. Ordnung}
		Eine lineare DGL 1. Ordnung hat die allgemeine Form
		\cenv{$y' = f(x,y)$}
		Dabei ist $f: \\real^2 \supset \Omega \to \\real$ eine gegebene Funktion, welche in jedem Punkt $(x,y)$
		die "Steigung" von $y$ angibt. $f$ ist quasi ein Richtungsfeld auf $\Omega$.
		Gesucht ist die Funktion $y(x)$, deren Graph in jedem Punkt $(x,y) \in \Omega$ die Steigung $f(x,y)$ hat.
		Die Lösungen der DGL bilden eine einparametrige Funktionenschar. Durche jeden Punkt $(x,y) \in \Omega$ geht genau
		eine Lösungskurve. Durch Formulierung eines Anfangswertproblems wird die Lösung eindeutig bestimmt.			

		\subsubsection{Polygonverfahren}
			Ein numerischer Lösungsansatz für Anfangswertprobleme 1. Ordnung.
			\cenv{$y' = f(x,y) ~ , ~ y(x_0) = y_0$}
			\begin{cenumerate}
				\item	Wähle eine Schrittweite $h > 0$
				\item	Für $k \geq 0$ setze rekursiv
					\cenv{$x_{k+1} := x_k + h$}
					\cenv{$y_{k+1} := y_k + f(x_k,y_k) \cdot h$}
			\end{cenumerate}
			Der Fehler ist umso kleiner, je kleiner die Schrittweite $h$ gewählt wurde und wächst exponentiell
			mit der Distanz des Punktes $x$ von $x_0$.

		\subsubsection{Allgemeine Lösung}
			\emph{Homogen}: $y' = p(x) \cdot y$ Die allgemeine Lösung der homogenen linearen DGL 1. Ordnung ist gegeben als
			\cenv{$y(x) = C \cdot e^{P(x)}, \qquad P(x) = \int p(x) ~ \diff x$}
			\emph{Inhomogen}: $y' = p(x) \cdot y + q(x)$ Die allgemeine Lösung erhält man durch \textbf{Variation der Konstanten}:\\
			\begin{cenumerate}
				\item	Löse die zugehörige homogene DGL: $\Rightarrow y_h(x) = C \cdot Y(x)$
				\item	Ansatz: $C$ als Funktion von $x$ auffassen: $y_p(x) = C(x) \cdot Y(x)$
				\item	Einsetzen des partikulären Ansatzes in die DGL, auflösen.
				\item	Überprüfen: Es sollte sich immer ein Teil wegkürzen.
			\end{cenumerate}

		\subsubsection{Separierbare DGL}
			DGL der Form $y' = g(x) \cdot k(y)$. Voraussetzung: $k(y) \neq 0$.\\
			\begin{cenumerate}
			\item	Schreibe DGL in der Form $\ddx{y} = g(x) \cdot k(y)$.
			\item	Separiere die Variablen: $\frac{1}{k(y)} \diff y = g(x) \cdot \diff x$
			\item	Integriere links nach $y$, rechts nach $x$, und zwar
				\begin{citemize}
				\item	unbestimmt, falls nach der allgemeinen Lösung gefragt ist. Dabei erscheint eine Integrationskonstante.
				\item	links von $y_0$ bis $y$, rechts von $x_0$ bis $x$, falls die durch $(x_0,y_0)$ gehende Lösung verlangt ist.
				\end{citemize}
			\item	Löse nach $y$ auf, falls möglich.
			\end{cenumerate}


	\subsection{Lineare DGL mit konstanten Koeffizienten}
		\subsubsection{Homogen}
			\cenv{$a_ny^{(n)} + a_{n-1}y^{(n-1)} + \ldots + a_0y = 0 \qquad (a_0, \ldots, a_n \in \real)$}
			Verwende \textbf{Euler-Ansatz}:
			\cenv{$y(x) = e^{\lambda x} \qquad (\lambda \in \complex)$}
			Löse \textbf{charakteristische Gleichung}:
			\cenv{$a_n\lambda^n+a_{n-1}\lambda^{n-1}+ \ldots + \lambda_0 = 0$}
			\emph{Reelle einfache Eigenwerte}: Alle Eigenwerte sind reell und $\lambda_i \neq \lambda_j$ wenn $i \neq j$. Dann sind
			\cenv{$y_i(t) =e^{\lambda_i t} \text{ und } y_j(t) =e^{\lambda_j t}$}
			Lösungen.\\
			\emph{Komplexe Eigenwerte}: Treten immer nur komplex konjugiert auf. Für jeden komplex konjugierten Eigenwert
			$\lambda = a \pm ib$ gibt es zwei linear unabhängige Lösungen mit reellen Koeffizienten:
			\cenv{$y_1(t) = e^{at} \cdot \cos(bt) ~ , ~ y_2(t) = e^{at} \cdot \sin(bt)$}
			\emph{Mehrfache Eigenwerte}: Jede $m$-fache Nullstelle $\lambda$ hat $m$ linear unabhängige Lösungen:
			\cenv{$e^{\lambda x}, xe^{\lambda x}, \ldots, x^{m-1}e^{\lambda x}$}
			\textbf{Superpositionsprinzip}: Sind die Funktionen $y_k$ linear unabhängige Lösungen, dann ist auch
			$y(t) = c_1 y_1(t) + \ldots + c_ky_k(t)$
			eine Lösung.\\
			\hlty{Konstanten $c_i \in \\real$ nicht vergessen!}

		\subsubsection{Inhomogen}
			\emph{Prinzip}: Seien $Y_1(t)$, $Y_2(t)$, \ldots , $Y_n(t)$ Lösungen der DGL $Ly = 0$,
			und sei $y_p(t)$ eine irgendwie gefundene Lösung der inhomogenen DGL $Ly = K(t)$. Dann ist 
			\cenv{$y(t) = c_1 Y_1(t) + c_2 Y_2(t) + \ldots + c_k Y_k(t) + y_p(t)$}
			die allgemeine Lösung von $Ly = K(t)$.\\
			\textbf{Superpositionsprinzip}: Sei $K(t) = K_1(t) + K_2(t)$. Falls $y_i$ eine Lösung der DGL $L_y = K_i(t)$ für $i = 1,2$ ist,
			ist $y_1 + y_2$ eine Lösung der DGL $Ly = K$.\\
			\emph{Ansatz mit unbestimmten Koeffizienten}: Die Anregung ist selber Lösung einer homogenen DGL mit konstanten Koeffizienten
			oder eine Linearkombination von solchen.\\
			\begin{quote}
				\emph{"$y_p(x)$ hat dieselbe Form wie der inhomogene Term $b(x)$."}
			\end{quote}
			\begin{center}
				\resizebox{\columnwidth}{!}{
					\begin{tabular}{| l | l | l |}
						\hline
						$b(x)$ & Spektralbedingung & Ansatz \\ \hline
						\multirow{2}{*}{$e^{\lambda_0x}$} & $\lambda_0 \notin$ Spec L & $y_p(x) = ae^{\lambda_0x}$ \\ \cline{2-3}
												       & $\lambda_0 \in$ Spec L ($m$-fach) & $y_p(x) = ax^me^{\lambda_0}$ \\ \hline
						\multirow{2}{*}{$\cos \omega x$ oder $\sin \omega x$} & $\pm i\omega \notin$ Spec L &
						$y_p(x) = a \cos \omega x + b \sin \omega x$ \\ \cline{2-3}
						& $\pm i\omega \in$ Spec L ($m$-fach) & $y_p(x) = x^m (a \cos \omega x + b \sin \omega x)$ \\ \hline
						\multirow{2}{*}{$x^r$} & $0 \notin$ Spec L & $y_p(x) = a_0 + a_1x + \ldots + a_rx^r$ \\ \cline{2-3}
											    & $0 \in$ Spec L ($m$-fach) & $y_p(x) = x^m (a_0 + a_1x + \ldots + a_rx^r)$ \\ \hline
						\multirow{2}{*}{$x^r e^{\lambda_0 x}$} & $\lambda_0 \notin$ Spec L &
						$y_p(x)=(a_0 + a_1x + \ldots + a_rx^r) e^{\lambda_0 x}$ \\ \cline{2-3}
						& $\lambda_0 \in$ Spec L & $y_p(x) = x^m (a_0 + a_1x + \ldots + a_rx^r) e^{\lambda_0 x}$ \\ \hline
		
					\end{tabular}
				}
			\end{center}
			\emph{Vorgehen}:
			\begin{cenumerate}
				\item	Inhomogenen Teil aufteilen
				\item	Für jeden inhomogenen Teil einen Ansatz aus der Tabelle wählen
				\item	Den Ansatz in die (auf der rechten Seite reduzierte) DGL einsetzen, Koeffizientenvergleich machen
				\item	So fortfahren für den Rest der inhomogenen Terme
				\item	Zusammensetzen: $y(x) = c_1 \cdot y_{h_1}(x) + \ldots + c_m \cdot y_{h_m}(x) + y_{p_1}(x) + \ldots + y_{p_n}(x)$
					(Für $m$ homogene, $n$ partikuläre Lsg.)
				\item	Anfangsbedingungen einsetzen
			\end{cenumerate}

\vfill
	\subsection{Eulersche DGL}
		Eine \textbf{Eulersche Differentialgleichung} ist eine \emph{lineare} homogene DGL der Form
		\cenv{$c_n y^{(n)} + \frac{c_{n-1}}{x} y^{(n-1)} + \frac{c_{n-2}}{x^2} y^{(n-2)}
		+ \ldots + \frac{c_1}{x^{n-1}} y' + \frac{c_0}{x^n} y = 0$}
		oder in alternativer Schreibweise
		\cenv{$c_n x^n y^{(n)} + c_{n-1}x^{n-1} y^{(n-1)} + \ldots + c_1 x y' + c_0 y = 0$}
		\emph{Homogen:}
		\begin{citemize}
			\item		Bringe DGL auf obige Form ("alternative Schreibweise")
			\item		Substituiere $y(x) = x^\alpha$, $x^\alpha$ kann direkt ausgeklammert werden
			\item		Löse $c_n \alpha^{(n)} + c_{n-1}\alpha^{(n-1)} + \ldots + c_1\alpha + c_0 = 0$\\
						Wobei $\alpha^{(n)}$ ein Polynom bezeichnet in der Hilfvariablen $\alpha$.\\
						$\alpha^{(k)} = \begin{dcases}
											0 & (k=0)\\
											\alpha \cdot(\alpha-1) \cdot(\alpha-2) \cdot\dots \cdot(\alpha-(k-1)) & (k>0)
										\end{dcases}$
			\item		Für $n$ verschiedene reelle Lösungen existieren $n$ linear unabhängige Lösungen:
				\cenv{$Y_k(x) := x^{\alpha_k} \qquad (1 \leq k \leq n)$}
			\item		Die allgemeine Lösung ist dann
				\cenv{$y(x) := c_1x^{\alpha_1} + c_2x^{\alpha_2} + \ldots + c_nx^{\alpha_n}$}
			\item		Eine \emph{doppelte Nullstelle} $\alpha_0$ hat die Lösungen
				\cenv{$Y_1(x) := x^{\alpha_0}, \quad Y_2(x) := x^{\alpha_0} \log(x)$}
		\end{citemize}
	
		\emph{Inhomogen:}
		\begin{citemize}
			\item		Substituiere $x = e^t$, $xy'(t) = \dot{y}$, $x^2y''(t) = \ddot{y} - \dot{y}$
			\item		Löse inhomogene DGL mit konstanten Koeff. nach $y(t)$.
			\item		Rücktransformation: $t = \log(x)$
		\end{citemize}			

