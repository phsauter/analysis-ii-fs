\section{Differentialrechnung in einer Variablen}
	\subsection{Ableitung}
		Sei $f: X \to \real$, $X \in \real$ eine Funktion und sei $x_0 \in X$. Falls der Grenzwert
		\fcenv{$f^\prime(x_0) := \lim\limits_{h \to 0} \frac{f(x_0 + h) - f(x_0)}{h}$}			
		existiert, heisst $f$ \textbf{differenzierbar an der Stelle} $x_0$
		und $f^\prime(x_0)$ ist die \textbf{Ableitung} von $f$ an der Stelle $x_0$.
		$f$ ist \textbf{differenzierbar} (auf $X$), wenn die Ableitung in jedem Punkt von $X$ existiert.\\
	
		Falls der Grenzwert nicht existiert, kann analog zu den Grenzwerten die \textbf{links- und rechtsseitige Ableitung} definiert werden.\\
	
		Eine Funktion ist \textbf{stetig differenzierbar} an der Stelle $x_0$, wenn die links- und rechtsseitige Ableitung in $x_0$ übereinstimmen.	


	\subsection{Rechnen mit Ableitungen}
		Seien $f$ und $g$ differenzierbar. Dann gelten folgende Regeln:
		\begin{tabular}{ll}
			\textbf{Faktorregel}					&	$(C \cdot f)^\prime = C \cdot f^\prime \qquad C = \text{const.}$ \\
			\textbf{Summenregel}					&	$(\alpha \cdot f + \beta \cdot g)^\prime
			= \alpha \cdot f^\prime + \beta \cdot g^\prime
			\qquad \forall \alpha ,\beta \in \real$ \\
			\textbf{Produktregel}					&	$(f \cdot g)^\prime = f^\prime \cdot g + f \cdot g^\prime$ \\
			\textbf{Quotientenregel}				&	$\left(\frac{f}{g}\right)^\prime
			= \frac{f^\prime \cdot g - f \cdot g^\prime}{g^2}$ \\
			\textbf{Kettenregel}					&	$g(f(x))^\prime = g^\prime(f(x)) \cdot f^\prime (x)$ \\
			\textbf{Log. Ableitung}					&	$(\log f)^\prime = \frac{f^\prime}{f}$ \\
			\textbf{Umkehrfunktion}					&	$(f^{-1}(y))^\prime = \frac{1}{f^\prime(f^{-1}(y))}$
		\end{tabular}


	\subsection{Zwischenwertsatz}
		Sei $f: [a,b] \to Y \subset \real$ eine stetige Funktion und sei $f(a) < 0 < f(b)$.
		Dann besitzt $f$ \ul{mindestens} eine Nullstelle $x_0 \in (a,b)$			
	
	
	\subsection{Kurvendiskussion}
		ACHTUNG: Immer Definitionsbereich bzw. Randpunkte beachten!
		\emph{Begriffe}\\
		\begin{tabular}{ll}
			\textbf{Konvex}				&	\(f''\geq 0\)								\\
			\textbf{Konkav}				&	\(f''\leq 0\)								\\
			\\
			Lokale Extrema:\\
			\textbf{lokales Maximum}		&	\(f'=0\) und \(f''<0\)							\\
			\textbf{lokales Minimum}		&	\(f'=0\) und \(f''>0\)							\\
			\\
			\textbf{Wendepunkte}		&	\(f''=0\)									\\
			\textbf{Sattelpunkte}			&	\(f'=0\) und \(f''=0\)							\\
			\\
			Globale Extrema:\\
			\textbf{Globales Maximum}	&	\(\max(\text{lokale Maxima}, \text{Randpunkte})\)	\\
			\textbf{Globales Minimum}		&	\(\min(\text{lokale Minima}, \text{Randpunkte})\)		\\
		\end{tabular}		

	\subsection{Mittelwertsatz}		
		Sei $f: [a,b] \to \real$ stetig und  auf $(a,b)$ differenzierbar. Dann gilt
		\fcenv{$\exists t \in (a,b): \qquad f'(t) = \frac{f(b) - f(a)}{b-a}$}


	\subsection{Taylor-Approximation}
		Es gilt $f(x) = T_{x_0}^n f(x) + R_{x_0}^n f(x)$.\\
		\textbf{n-tes Taylorpolynom} von $ f $ an der Stelle $ x_0 $
		\fcenv{$T_{x_0}^{n} f(x) = \smashoperator{\sum \limits_{k=0}^{n}} \frac{f^{(k)}(x_0)}{k!} \cdot (x-x_0)^k $}

		\textbf{n-tes Restglied}
		\cenv{$\exists t \in (x, x_0): \qquad R_{x_0}^{n} f(x) = \frac{f^{(n+1)}(t)}{(n+1)!} \cdot (x-x_0)^{n+1}$}
		\emph{Vorsicht!} Das "Intervall" der Reihenentwicklung muss mitberücksichtigt werden! Bsp. $\sin$ mit $x^{2n+1}$, den Grad $2n+1$
		unverändert in die Formel einsetzen, ein Auflösen nach $n$ macht hier keinen Sinn!\\
	
		\emph{Tipp}: Für Abschätzungen kann es nützlich sein, die Beschränktheit von Funktionen auszunützen,
		z.B. $\abs{\sin(x)} \leq 1$.


	\subsection{Punktweise Konvergenz}
	\label{sec:punktw-konv}
		Eine Folge stetiger Funktionen $f_n : \Omega \subset \real \to \real$ 
		\textbf{konvergiert punktweise} gegen $f(x)$, falls
		\cenv{$\forall x \in \Omega \quad \lim\limits_{n\to \infty} f_n(x) = f(x)$}
		
		
	\subsection{Gleichmässige Konvergenz}
		\label{sec:gleichm-konv}
		Eine Folge stetiger Funktionen $f_n : \Omega \subset \real \to \real$ 
		\textbf{konvergiert gleichmässig} gegen $f(x)$, falls
		\cenv{$\lim\limits_{n \to \infty} \sup_{x\in \Omega} \abs{ f_n(x)-f(x)} = 0$}
	
		\bigskip
	
		\emph{Rezept}:\\
		Gegeben: Folge stetiger Funktionen $f_n : \Omega \subset \real \to \real$\\
		Gefragt: Konvergiert $f_n$ auf $\Omega$ gleichmässig?\\
		\textbf{Schritt 1:} Berechne den Limes von $f_n$ auf $\Omega$, d.h.
		$f(x) = \lim\limits_{n \to \infty} f_n(x)$.\\
		\textbf{Schritt 2:} Prüfe $f_n$ auf gleichmässige Konvergenz.\\
		\underline{Direkte Methode:}
		\begin{enumerate}
			\item Berechne $\sup_{x\in \Omega} \abs{f_n(x)  -f(x)}$.\\
				Zu diesem Zweck ist es oft nützlich die Ableitung nach $x$ von
				$\abs{f_n(x) -f(x)}$ zu berechnen und diese gleich Null zu setzen.
			\item Bilde den Limes für $n \to \infty$:\\
				\cenv{$\lim\limits_{n\to \infty} \sup_{x\in \Omega} \abs{f_n(x)-f(x)}$}
				Gilt $\lim\limits_{n \to \infty} \sup_{x\in \Omega} \abs{f_n(x)-f(x)} = 0$ 
				so ist $f_n$ auf $\Omega$ gleichmässig konvergent.
		\end{enumerate}
	
		\underline{Indirekte Methoden:}
		\begin{itemize}
			\item $ f$ unstetig $\Rightarrow$ keine gleichmässige Konvergenz.
			\item $f$ stetig, $f_n(x) \leq f_{n+1}(x) \forall x \in \Omega$ und
				$\Omega$ kompakt $\Rightarrow$ gleichmässige Konvergenz.
		\end{itemize}

	% \subsubsection{Newton-Verfahren}
	% 	\begin{center}
	% 		\includegraphics[width=4cm]{img/newton.png}
	% 	\end{center}
	% 	Beginne mit einem Punkt $ x_0 \in I $. Der nächste Punkt ist
	% 		\eqbox{x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)}}

